package eu.ncdc.sumervacation;

import java.util.ArrayList;
import java.util.List;

public class Configs 
{
	private String jiraUserName;
	private String jiraPassword;
	private String userName;
	
	private String emailFrom;
	private String emailTo;

	private String calendarId;
	
	private List<String> guestEmails;

	public Configs()
	{
		guestEmails = new ArrayList<String>();
	}
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmailFrom() {
		return emailFrom;
	}

	public void setEmailFrom(String emailFrom) {
		this.emailFrom = emailFrom;
	}

	public String getCalendarId() {
		return calendarId;
	}

	public void setCalendarId(String calendarId) {
		this.calendarId = calendarId;
	}
	
	public String getJiraUserName() 
	{
		return jiraUserName;
	}
	
	public void setJiraUserName(String jiraUserName) 
	{
		this.jiraUserName = jiraUserName;
	}
	
	public String getJiraPassword() 
	{
		return jiraPassword;
	}
	
	public void setJiraPassword(String jiraPassword) 
	{
		this.jiraPassword = jiraPassword;
	}

	public List<String> getGuestEmails() {
		return guestEmails;
	}

	public void setGuestEmails(List<String> guestEmails) {
		this.guestEmails = guestEmails;
	}
	
	public String getEmailTo() {
		return emailTo;
	}

	public void setEmailTo(String emailTo) {
		this.emailTo = emailTo;
	}

}
