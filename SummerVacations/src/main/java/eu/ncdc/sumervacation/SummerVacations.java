package eu.ncdc.sumervacation;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.mail.MessagingException;

import com.beust.jcommander.JCommander;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.apache.GoogleApacheHttpTransport;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.CalendarScopes;

public class SummerVacations 
{
	private static final String APPLICATION_NAME = "Vacations Helper";
	private static final File DATA_STORE_DIR = new File(System.getProperty("user.home", ".credentials/calendar-java-vacHeleper"));
	private static FileDataStoreFactory DATA_STORY_FACTORY;
	private static JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
	private static HttpTransport HTTP_TRANSPORT;
	
	private static final List<String> SCOPES = Arrays.asList(CalendarScopes.CALENDAR);
	
	static 
	{
		try
		{
			HTTP_TRANSPORT = GoogleApacheHttpTransport.newTrustedTransport();
			DATA_STORY_FACTORY = new FileDataStoreFactory(DATA_STORE_DIR); 
		}
		catch (Throwable t)
		{
			t.printStackTrace();
			System.exit(1);
		}
	}
	
	public static Credential authorize() throws IOException
	{
		InputStream in = SummerVacations.class.getResourceAsStream("/client_secret.json");
		GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));
		
		GoogleAuthorizationCodeFlow flow = 
				new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
					.setDataStoreFactory(DATA_STORY_FACTORY)
					.setAccessType("offline")
					.build();
		
		Credential credentil = 
				new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver())
					.authorize("user");
		
		System.out.println("Credentials saved to " + DATA_STORE_DIR.getAbsolutePath());
		
		return credentil;
	}
	
	public static Calendar getCalendarService() throws IOException
	{
		Credential credential = authorize();
				
		return new Calendar.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential)
					.setApplicationName(APPLICATION_NAME)
					.build();
	}
	
	public static void main(String[] args) throws IOException, ParseException, MessagingException
	{
		Calendar calendar = getCalendarService();
		Configs configs = new Configs();
		
		configs.setJiraUserName("zoltmac");
		configs.setJiraPassword("praktyki");
		configs.setEmailFrom("ncdc.competition.hub.mail.bot@gmail.com");
		configs.setEmailTo("eddieirvine696@gmail.com");
		configs.setCalendarId("primary");
		configs.setUserName(configs.getJiraUserName());
		
		List<String> emails = new ArrayList<String>();
		
		emails.add("zoltmac@hot.ncdc.lan");
		
		configs.setGuestEmails(emails);
				
		UserInterface userInterface = new UserInterface(calendar, configs);

		CMD cmd = new CMD();
		JCommander jCommander = new JCommander(cmd, args);
		
		if (cmd.isClear())
		{
			userInterface.deleteAllWorklogs();
			userInterface.deleteAllEvents();
			
			return;
		}
		
		if (cmd.isShow())
			userInterface.showVacations();
		
		if (cmd.getAdd() != null && cmd.getAdd().size() > 1)
			userInterface.addVacation(cmd.getAdd().get(0), cmd.getAdd().get(1));
		
		if (cmd.getDelete() > 0 && cmd.getDelete() <= userInterface.getUserVacations().getVacationEvents().size())
			userInterface.deleteVacation(cmd.getDelete() - 1);
		
	}
}
