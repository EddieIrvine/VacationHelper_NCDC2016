package eu.ncdc.summervacation.jiraenties;

public class DeleteWorklog 
{
	private String adjustEstimate;
	private String newEstimate;
	private String increaseBy;
	
	public String getAdjustEstimate() 
	{
		return adjustEstimate;
	}
	
	public void setAdjustEstimate(String adjustEstimate) 
	{
		this.adjustEstimate = adjustEstimate;
	}
	
	public String getNewEstimate() 
	{
		return newEstimate;
	}
	
	public void setNewEstimate(String newEstimate) 
	{
		this.newEstimate = newEstimate;
	}
	
	public String getIncreaseBy() 
	{
		return increaseBy;
	}
	
	public void setIncreaseBy(String increaseBy) 
	{
		this.increaseBy = increaseBy;
	}
	
}
