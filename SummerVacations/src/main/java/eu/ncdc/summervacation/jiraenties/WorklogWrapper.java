package eu.ncdc.summervacation.jiraenties;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WorklogWrapper
{
	private Long startAt;
	private List<Worklog> worklogs;
	
	public Long getStartAt() 
	{
		return startAt;
	}
	
	public void setStartAt(Long startAt) 
	{
		this.startAt = startAt;
	}
	
	public List<Worklog> getWorklogs() 
	{
		return worklogs;
	}
	
	public void setWorklogs(List<Worklog> worklogs) 
	{
		this.worklogs = worklogs;
	}
}
