package eu.ncdc.summervacation.jiraenties;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Search 
{
	private List<Issue> issues;

	public List<Issue> getIssues() 
	{
		return issues;
	}

	public void setIssues(List<Issue> issues) 
	{
		this.issues = issues;
	}
}
