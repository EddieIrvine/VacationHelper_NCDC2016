package eu.ncdc.summervacation.enties;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Scanner;

import javax.xml.bind.DatatypeConverter;

import org.codehaus.jackson.map.ObjectMapper;

@SuppressWarnings("restriction")
public class JiraResource 
{
	private String jiraUrl;
	private String userName;
	private String password;
	
	public JiraResource(String Url, String userName, String password)
	{
		this.jiraUrl = Url;
		this.userName = userName;
		this.password = password;
	}
	
	public String getUrl() 
	{
		return jiraUrl;
	}

	public void setUrl(String url) 
	{
		jiraUrl = url;
	}

	public String getUserName() 
	{
		return userName;
	}

	public void setUserName(String userName) 
	{
		this.userName = userName;
	}

	public String getPassword() 
	{
		return password;
	}

	public void setPassword(String password) 
	{
		this.password = password;
	}

	public HttpURLConnection connection(String Url) throws MalformedURLException, IOException
	{
		HttpURLConnection connection = (HttpURLConnection) new URL(jiraUrl+"/"+Url).openConnection();
		
		String tmp = userName+":"+password;
		byte[] tmpB = tmp.getBytes();
		String headerAuthorization = DatatypeConverter.printBase64Binary(tmpB);
		
		connection.setRequestProperty("Authorization", "Basic "+headerAuthorization);
		connection.setRequestProperty("Content-Type", "application/json");
		
		return connection;
	}
	
	@SuppressWarnings("resource")
	public void printInputStream(InputStream is)
	{
		Scanner scanner = null;
		
		try
		{
			scanner = new Scanner(is).useDelimiter("\\A");
			
			String result = scanner.hasNext() ? scanner.next() : "";
			
			System.out.println("INPUTSTREAM IS: " + result);
		}
		finally
		{
			if (scanner != null)
			{
				scanner.close();
			}
		}
	}
	
	public List<Worklog> getWorklogs() throws MalformedURLException, IOException
	{
		HttpURLConnection connection = connection("rest/api/2/issue/VAC-4/worklog");
		
		connection.setRequestMethod("GET");
		connection.setDoInput(true);
		
		InputStream is = connection.getInputStream();
		
		ObjectMapper mapper = new ObjectMapper();
		WorklogWrapper ww = mapper.readValue(is, WorklogWrapper.class);
		
		return ww.getWorklogs();
	}
}
