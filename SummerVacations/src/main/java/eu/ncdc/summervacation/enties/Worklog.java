package eu.ncdc.summervacation.enties;

import java.util.Date;

public class Worklog 
{	
	private Date started;
	private Long timeSpentSeconds;
	private String comment;
	
	@Override
	public String toString()
	{
		return String.format("%s\n%d\n%s", started.toString(), timeSpentSeconds, comment);
	}
	
	public Date getStarted() 
	{
		return started;
	}
	
	public void setStarted(Date started) 
	{
		this.started = started;
	}
	
	public Long getTimeSpentSeconds() 
	{
		return timeSpentSeconds;
	}
	
	public void setTimeSpentSeconds(Long timeSpentSeconds) 
	{
		this.timeSpentSeconds = timeSpentSeconds;
	}
	
	public String getComment() 
	{
		return comment;
	}
	
	public void setComment(String comment) 
	{
		this.comment = comment;
	}
}
