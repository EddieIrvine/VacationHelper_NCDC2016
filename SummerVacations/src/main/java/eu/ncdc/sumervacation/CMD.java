package eu.ncdc.sumervacation;

import java.util.ArrayList;
import java.util.List;

import com.beust.jcommander.Parameter;

public class CMD 
{
	@Parameter
	private List<String> parameters = new ArrayList<String>();
	
	@Parameter(names = "-a", description = "Add vacation <start date> <end date> in yyyy-MM-dd format")
	private List<String> add;

	@Parameter(names = "-d", description = "Delete vacation <number of vacations>")
	private int delete;
	
	@Parameter(names = "-s", arity = 1, description = "Show my vacations from this day")
	private boolean show = false;
	
	@Parameter(names = "-c")
	private boolean clear = false;

	public boolean isClear() {
		return clear;
	}

	public void setClear(boolean clear) {
		this.clear = clear;
	}

	public List<String> getAdd() {
		return add;
	}

	public void setAdd(List<String> add) {
		this.add = add;
	}

	public int getDelete() {
		return delete;
	}

	public void setDelete(int delete) {
		this.delete = delete;
	}

	public boolean isShow() {
		return show;
	}

	public void setShow(boolean show) {
		this.show = show;
	}
		
	
}
