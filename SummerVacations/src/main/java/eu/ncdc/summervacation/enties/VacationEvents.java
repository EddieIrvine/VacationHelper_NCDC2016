package eu.ncdc.summervacation.enties;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;

import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.Event;

import eu.ncdc.sumervacation.Configs;
import eu.ncdc.summervacation.jiraenties.Issue;
import eu.ncdc.summervacation.jiraenties.JiraResource;
import eu.ncdc.summervacation.jiraenties.Search;
import eu.ncdc.summervacation.jiraenties.SearchQuery;

public class VacationEvents 
{
	private Configs configs;

	private String userName;
	private String jiraPassword;
	
	private JiraResource jiraResource;
	private ObjectMapper mapper;
	
	private Calendar calendar;
	private List<Vacation> vacations;
	
	private List<Issue> mainTasks;
	private List<Issue> subTasks;
	
	private int currentYear;
	
	public int getCurrentYear() {
		return currentYear;
	}

	public void setCurrentYear(int currentYear) {
		this.currentYear = currentYear;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public Issue getSubTask(int index)
	{
		return subTasks.get(index);
	}
	
	public List<Issue> getMainTasks() 
	{
		return mainTasks;
	}

	public void setMainTasks(List<Issue> mainTasks) 
	{
		this.mainTasks = mainTasks;
	}

	public VacationEvents(Calendar calendar, Configs configs) throws IOException, ParseException
	{		
		this.configs = configs;
		this.calendar = calendar;
		this.userName = configs.getJiraUserName();
		this.jiraPassword = configs.getJiraPassword();
		
		this.currentYear = new GregorianCalendar().get(GregorianCalendar.YEAR);
		
		jiraResource = new JiraResource("http://koko.ncdc/jira", userName, jiraPassword);
		mapper = new ObjectMapper();
		
		mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ"));
		
		int year = new GregorianCalendar().get(GregorianCalendar.YEAR);
		
		mainTasks = getIssues(year); 
		subTasks = new ArrayList<Issue>();
		
		List<Issue> subTasksTemp = getIssues();
		
		for (Issue issue : subTasksTemp)
		{
			Issue newSub = getSubTask(issue.getSelf());
			
			newSub.setYear();
			
			if (newSub.getYear() >= currentYear)
				subTasks.add(newSub);
		}
		
		DateTime currentTime = new DateTime(System.currentTimeMillis());
		
		com.google.api.services.calendar.model.Events events = this.calendar.events().list("primary")
				.setTimeMin(currentTime)
				.setOrderBy("startTime")
				.setSingleEvents(true)
				.execute();
		
		List<Event> items = events.getItems();
		
		vacations = new ArrayList<Vacation>();
			
		for (Event event : items)
		{
			DateTime start = event.getStart().getDateTime();
				
			if (start == null)
				start = event.getStart().getDate();
				
			Vacation vac = new Vacation(userName, jiraPassword);
				
			vac.setCalendarID(configs.getCalendarId());
			vac.seteMailFrom(configs.getEmailFrom());
			vac.seteMailTo(configs.getEmailTo());
			vac.setEventID(event.getId());
			vac.setSummary(event.getSummary());
			vac.setDescription(event.getDescription());
				
			vac.setStart(event.getStart().getDateTime().toString());
			vac.setEnd(event.getEnd().getDateTime().toString());
			
			vacations.add(vac);
		}
	}
	
	public Vacation getEvent(int index)
	{
		return vacations.get(index);
	}
	
	public List<Vacation> getVacationEvents()
	{
		return vacations;
	}
	
	public void add(Vacation vac)
	{
		vacations.add(vac);
	}
	
	public void remove(String id) throws IOException, ParseException
	{
		for (Vacation vac : vacations)
		{
			if (vac.getEventID().equals(id))
			{
				vac.deleteEvent(calendar);
				vacations.remove(vac);
							
				break;
			}
		}
		
		System.out.println("Remove success!");
	}
	
	public int getNumberOfVacations()
	{
		return vacations.size();
	}
	
	public void showVacations()
	{
		if (vacations.size() == 0)
		{
			System.out.println("You have no planned vacations :-(");
		}
		else
		{
			System.out.println("You have " + vacations.size() + " planned vacations :-)\n");
			
			for (Vacation vac : vacations)
			{
				System.out.println(vac.toString());
			}
		}
	}
	
	public Issue getSubTask(String self) throws MalformedURLException, IOException
	{		
		HttpURLConnection connection = jiraResource.fullURLConnection(self);
		
		connection.setRequestMethod("GET");
		connection.setDoInput(true);
		
		InputStream is = connection.getInputStream();
		Issue newSubTask = mapper.readValue(is, Issue.class);
		
		connection.disconnect();
		
		return newSubTask;
	}
	
	public List<Issue> getIssues(int year) throws MalformedURLException, IOException
	{
		HttpURLConnection connection = jiraResource.connection("rest/api/2/search");
		
		connection.setRequestMethod("POST");
		connection.setDoOutput(true);
		connection.setDoInput(true);
		
		SearchQuery searchQuery = new SearchQuery();
		List<String> fields = new ArrayList<String>();
		
		fields.add("id");
		fields.add("key");
		fields.add("summary");
		
		int nextYear = year + 1;
		
		searchQuery.setJql("summary ~ Vacations AND summary ~ " + year + " OR summary ~ " + nextYear);
		searchQuery.setFields(fields);
		searchQuery.setStartAt(0);
		
		String json = mapper.writeValueAsString(searchQuery);
		
		OutputStream os = connection.getOutputStream();
		
		os.write(json.getBytes());
		os.flush();
			
		InputStream is = connection.getInputStream();
		
		Search search = mapper.readValue(is, Search.class);
		
		List<Issue> issues = search.getIssues();
		
		connection.disconnect();
		
		return issues;
	}
	
	public String getSubTaskKey(int year)
	{
		String key = null;
		
		for (Issue issue : subTasks)
		{
			if (issue.getYear() == year)
				return issue.getKey();
		}
		
		return key;
	}
	
	public List<Issue> getIssues() throws MalformedURLException, IOException
	{
		HttpURLConnection connection = jiraResource.connection("rest/api/2/search");
		
		connection.setRequestMethod("POST");
		connection.setDoOutput(true);
		connection.setDoInput(true);
		
		SearchQuery searchQuery = new SearchQuery();
		List<String> fields = new ArrayList<String>();
		
		fields.add("id");
		fields.add("key");
		fields.add("summary");
		fields.add("parent");
		
		String jql = "summary ~ Vacation AND summary ~ " + userName;
		
		searchQuery.setJql(jql);
		searchQuery.setFields(fields);
		searchQuery.setStartAt(0);
		
		String json = mapper.writeValueAsString(searchQuery);
		
		OutputStream os = connection.getOutputStream();
		
		os.write(json.getBytes());
		os.flush();
				
		InputStream is = connection.getInputStream();
		
		Search search = mapper.readValue(is, Search.class);
		
		List<Issue> issues = search.getIssues();
		
		connection.disconnect();
		
		return issues;
	}

	public List<Issue> getSubTasks() {
		return subTasks;
	}

	public void setSubTasks(List<Issue> subTasks) {
		this.subTasks = subTasks;
	}
	
	public String getJiraPassword() {
		return jiraPassword;
	}

	public void setJiraPassword(String jiraPassword) {
		this.jiraPassword = jiraPassword;
	}
	
	public Configs getConfigs() {
		return configs;
	}

	public void setConfigs(Configs configs) {
		this.configs = configs;
	}
}
