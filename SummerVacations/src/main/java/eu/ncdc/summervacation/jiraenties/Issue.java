package eu.ncdc.summervacation.jiraenties;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Issue 
{
	private String id;
	private String self;
	private String key;
	
	private Fields fields;
	
	private int year;
	
	public void setYear()
	{
		String parentSummary = fields.getParent().getFields().getSummary();
		
		//za tysi�c lat zamie� "2" na "3" :-)
		year = Integer.parseInt(parentSummary.substring(parentSummary.indexOf("2"), parentSummary.length()));
	}
	
	public int getYear()
	{
		return year;
	}
	
	public Fields getFields() {
		return fields;
	}

	public void setFields(Fields fields) {
		this.fields = fields;
	}

	public String toString()
	{
		return String.format("%s\n%s\n%s\n%s\n", id, self, key, fields.getSummary());
	}
	
	public String getId() 
	{
		return id;
	}
	
	public void setId(String id) 
	{
		this.id = id;
	}
	
	public String getSelf() 
	{
		return self;
	}
	
	public void setSelf(String self) 
	{
		this.self = self;
	}

	public String getKey() 
	{
		return key;
	}

	public void setKey(String key) 
	{
		this.key = key;
	}

}

@JsonIgnoreProperties(ignoreUnknown = true)
class Fields
{
	private String summary;
	private Parent parent;
	
	public Parent getParent() {
		return parent;
	}

	public void setParent(Parent parent) {
		this.parent = parent;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}	
}

@JsonIgnoreProperties(ignoreUnknown = true)
class Parent
{
	private Fields fields;

	public Fields getFields() {
		return fields;
	}

	public void setFields(Fields fields) {
		this.fields = fields;
	}
}