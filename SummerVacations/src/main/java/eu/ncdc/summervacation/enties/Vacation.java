package eu.ncdc.summervacation.enties;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.codehaus.jackson.map.ObjectMapper;

import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventAttendee;
import com.google.api.services.calendar.model.EventDateTime;

import eu.ncdc.summervacation.jiraenties.DeleteWorklog;
import eu.ncdc.summervacation.jiraenties.JiraResource;
import eu.ncdc.summervacation.jiraenties.Worklog;
import eu.ncdc.summervacation.jiraenties.WorklogWrapper;

public class Vacation
{	
	private String issueKey;
	private String eMailTo;
	private String eMailFrom;
	
	private ObjectMapper mapper;
	private JiraResource jiraResource;
	
	private String calendarID;
	private String eventID;
	
	private String summary, description;
	private String start, end;
	
	private List<Worklog> vacationWorklogs;
	private List<EventAttendee> attendees;
	
	private class SMTPAuthenticator extends Authenticator 
	{
        public PasswordAuthentication getPasswordAuthentication() 
        {
            return new PasswordAuthentication("ncdc.competition.hub.mail.bot@gmail.com", "praktyki");
        }
    }
	
	public Vacation(String name, String pass) throws MalformedURLException, IOException, ParseException
	{
		attendees = new ArrayList<EventAttendee>();
		
		vacationWorklogs = new ArrayList<Worklog>();
		jiraResource = new JiraResource("http://koko.ncdc/jira", name, pass);
		mapper = new ObjectMapper();
		
		mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ"));
	}
	
	public Vacation(String key)
	{
		super();
		
		issueKey = key;
	}
	
	public void setIssueKey(String issueKey)
	{
		this.issueKey = issueKey;
	}
	
	public String getIssueKey()
	{
		return issueKey;
	}
	
	public void addAttendee(EventAttendee attendee)
	{
		attendees.add(attendee);
	}
	
	public boolean isSynchronized() throws ParseException
	{
		Date dateStart = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(start);
				
		for (Worklog worklog : vacationWorklogs)
		{				
			GregorianCalendar workCalendar = new GregorianCalendar();
			
			workCalendar.setTime(dateStart);
					
			int weekDay = workCalendar.get(GregorianCalendar.DAY_OF_WEEK);
								
			if (weekDay != GregorianCalendar.SATURDAY && weekDay != GregorianCalendar.SUNDAY)
			{
				if (!dateStart.equals(worklog.getStarted()))
					return false;
			}
			
			if (weekDay != GregorianCalendar.FRIDAY) 
				workCalendar.add(GregorianCalendar.DATE, 1);
			else 
				workCalendar.add(GregorianCalendar.DATE, 3);
				
			dateStart = workCalendar.getTime();
		}
	
		return true;
	}
	
	@SuppressWarnings("deprecation")
	public void getWorklogs() throws MalformedURLException, IOException, ParseException
	{
		List<Worklog> worklogs = getAllVacationWorklogs();
		
		Date dateStart = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(start);
		Date dateEnd = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(end);
		
		dateEnd.setHours(8);
		
		for (Worklog w : worklogs)
		{
			Date started = w.getStarted();
			
			if ((dateStart.equals(started) || dateStart.before(started)) &&
				 (dateEnd.after(started) || dateEnd.equals(started)))
			{
				vacationWorklogs.add(w);
			}
		}
	}
	
	public static boolean isDateFree(Calendar calendar, String calendarID, String start, String end) throws IOException 
	{
		DateTime dateStart = new DateTime(start);
		DateTime dateEnd = new DateTime(end);
		
        com.google.api.services.calendar.model.Events events = calendar.events().list(calendarID)
                .setMaxResults(2)
                .setTimeMin(dateStart)
                .setTimeMax(dateEnd)
                .setOrderBy("startTime")
                .setSingleEvents(true)
                .execute();
     
        List<Event> items = events.getItems();


        if (items.size() == 0) 
        {
            return true;
        }
        else 
        {
            return false;
        }
    }
	
	public void addEvent(Calendar service) throws IOException, ParseException
	{
		Event event = new Event().setSummary(summary);
		
		event.setAttendees(attendees);
		
		if (description != null) event.setDescription(description);
		
		GregorianCalendar cal = new GregorianCalendar();
		Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(start);		
		
		cal.setTime(date);
		
		int weekDay = cal.get(GregorianCalendar.DAY_OF_WEEK);
		
		if (weekDay == GregorianCalendar.SATURDAY)
		{
			cal.add(GregorianCalendar.DATE, 2);
		}
		
		if (weekDay == GregorianCalendar.SUNDAY)
		{
			cal.add(GregorianCalendar.DATE, 1);
		}
		
		start = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").format(cal.getTime());
		
		date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(end);
		
		cal.setTime(date);
		
		weekDay = cal.get(GregorianCalendar.DAY_OF_WEEK);
		
		if (weekDay == GregorianCalendar.SATURDAY)
		{
			cal.add(GregorianCalendar.DATE, -1);
		}
		
		if (weekDay == GregorianCalendar.SUNDAY)
		{
			cal.add(GregorianCalendar.DATE, -2);
		}
		
		end = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").format(cal.getTime());
		
		DateTime dateStart = new DateTime(start);
		
		EventDateTime s = new EventDateTime().setDateTime(dateStart)
											 .setTimeZone("Europe/Warsaw");
		
		DateTime dateEnd = new DateTime(end);
		EventDateTime e = new EventDateTime().setDateTime(dateEnd)
											 .setTimeZone("Europe/Warsaw");

		event.setStart(s);
		event.setEnd(e);
		
		event = service.events().insert(calendarID, event).execute();
		
		System.out.printf("Event created: %s\n", event.getHtmlLink());
		
		eventID = event.getId();
	}
	
	public void deleteEvent(Calendar calendar) throws IOException, ParseException
	{
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		
		Date dateStart = format.parse(start);
				
		if (dateStart.before(new Date()))
		{
			calendar.events().delete(calendarID, eventID).execute();
			
			GregorianCalendar vacCal = new GregorianCalendar();
			
			int year = vacCal.get(GregorianCalendar.YEAR);
			int month = vacCal.get(GregorianCalendar.MONTH);
			int day = vacCal.get(GregorianCalendar.DAY_OF_MONTH) - 1;
			
			int hour = 8;
			int minute = 0;
			int seconds = 0;

			end = format.format(new GregorianCalendar(year, month, day, hour, minute, seconds).getTime());

			try
			{
				addEvent(calendar);
			}
			catch (Exception ex)
			{
				calendar.events().delete(calendarID,  eventID);
			}
		}
		else
			calendar.events().delete(calendarID, eventID).execute();
	}
	
	public void addWorklog(Worklog worklog) throws MalformedURLException, IOException
	{
		HttpURLConnection connection = jiraResource.connection("rest/api/2/issue/"+issueKey+"/worklog");
		
		String json = mapper.writeValueAsString(worklog);
		
		connection.setRequestMethod("POST");
		connection.setDoOutput(true);
		
		OutputStream os = connection.getOutputStream();
		
		os.write(json.getBytes("UTF-8"));
		os.flush();
		
		int responseCode = connection.getResponseCode();
		
		if (responseCode != 201)
		{
			System.err.println("Error " + responseCode + "!");
		}
		
		connection.disconnect();
	}
	
	@SuppressWarnings("deprecation")
	public void deleteWorklogs() throws MalformedURLException, IOException, ParseException
	{	
		Date todayDate = new Date();
			
//		GregorianCalendar date = new GregorianCalendar(2016, GregorianCalendar.DECEMBER, 30, 8, 0, 0);
//		
//		Date todayDate = date.getTime();
		
		List<Worklog> deletes = new ArrayList<Worklog>();
		
		for (Worklog worklog : vacationWorklogs)
		{
			Date started = worklog.getStarted();
			
			if (started.getYear() == todayDate.getYear() &&
				started.getMonth() == todayDate.getMonth() &&
				started.getDay() == todayDate.getDay())
			{
				deleteWorklog(worklog.getId());
				deletes.add(worklog);
				
				continue;
			}
		
			
			if (started.after(todayDate) || started.equals(todayDate))
			{
				deleteWorklog(worklog.getId());
				deletes.add(worklog);
			}
		}
		
		for (Worklog worklog : deletes)
		{
			vacationWorklogs.remove(worklog);
		}
	}
	
	public List<Worklog> getAllVacationWorklogs() throws MalformedURLException, IOException
	{
		List<Worklog> worklogs = new ArrayList<Worklog>();
		HttpURLConnection connection = jiraResource.connection("rest/api/2/issue/" + issueKey + "/worklog");
		
		InputStream inputStream = connection.getInputStream();
		WorklogWrapper ww = mapper.readValue(inputStream, WorklogWrapper.class);
		
		worklogs = ww.getWorklogs();
		
		connection.disconnect();
		
		return worklogs;
	}
	
	public void deleteWorklog(String id) throws MalformedURLException, IOException
	{
		HttpURLConnection connection = jiraResource.connection("rest/api/2/issue/" + issueKey + "/worklog/" + id);
		
		DeleteWorklog delWorklog = new DeleteWorklog();
		
		delWorklog.setAdjustEstimate("");
		delWorklog.setIncreaseBy("");
		delWorklog.setNewEstimate("");
		
		String json = mapper.writeValueAsString(delWorklog);
		
		connection.setRequestMethod("DELETE");
		connection.setDoOutput(true);
		
		OutputStream os = connection.getOutputStream();
		
		os.write(json.getBytes());
		os.flush();
		
		int responseCode = connection.getResponseCode();
		
		if (responseCode != 201)
		{
			
		}
		
		connection.disconnect();
	}
	
	public void emailSending(String name, int year, String text) throws MessagingException
	{
		Properties props = System.getProperties();
		
		props.put("mail.smtp.user", eMailFrom);
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");
		props.put("mail.smtp.starttls", "true");
		props.put("mail.smtp.debug", "true");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.socketFactory.port", "587");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.socketFactory.fallback", "false");
		
		SMTPAuthenticator auth = new SMTPAuthenticator();
		
		Session session = Session.getInstance(props, auth);
		
		session.setDebug(false);
		
		MimeMessage msg = new MimeMessage(session);
		
		msg.setText(toString());
		msg.setSubject(text);
		msg.setFrom(new InternetAddress(eMailFrom));
		
		msg.addRecipients(Message.RecipientType.TO, eMailTo);
		
		Transport transport = session.getTransport("smtps");
		
		transport.connect("smtp.gmail.com", 465, eMailFrom, "praktyki");
		transport.sendMessage(msg, msg.getAllRecipients());
		transport.close();
		
	}
	
	@Override
	public String toString()
	{
		return String.format("%s (%s - %s)", summary, start, end);
	}
	
	public String getSummary() 
	{
		return summary;
	}

	public void setSummary(String summary) 
	{
		this.summary = summary;
	}

	public String getDescription() 
	{
		return description;
	}

	public void setDescription(String description) 
	{
		this.description = description;
	}

	public String getStart() 
	{
		return start;
	}

	public void setStart(String start) 
	{
		this.start = start;
	}

	public String getEnd() 
	{
		return end;
	}

	public void setEnd(String end) 
	{
		this.end = end;
	}

	public String getEventID() 
	{
		return eventID;
	}

	public void setEventID(String eventID) 
	{
		this.eventID = eventID;
	}
	
	public JiraResource getJiraResource()
	{
		return jiraResource;
	}

	public List<Worklog> getVacationWorklogs()
	{
		return vacationWorklogs;
	}
	
	public void setVacationWorklogs(List<Worklog> vacationWorklogs) 
	{
		this.vacationWorklogs = vacationWorklogs;
	}

	public List<EventAttendee> getAttendees() 
	{
		return attendees;
	}

	public void setAttendee(List<EventAttendee> attendees) 
	{
		this.attendees = attendees;
	}
	
	
	public void setCalendarID(String calendarID)
	{
		this.calendarID = calendarID;
	}
	
	public String getCalendarID()
	{
		return calendarID;
	}

	public String geteMailTo() {
		return eMailTo;
	}

	public void seteMailTo(String eMailTo) {
		this.eMailTo = eMailTo;
	}

	public String geteMailFrom() {
		return eMailFrom;
	}

	public void seteMailFrom(String eMailFrom) {
		this.eMailFrom = eMailFrom;
	}
	
	
}
