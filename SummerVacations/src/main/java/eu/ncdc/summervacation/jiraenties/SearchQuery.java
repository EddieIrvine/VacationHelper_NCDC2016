package eu.ncdc.summervacation.jiraenties;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties("true")
public class SearchQuery 
{
	private String jql;
	private List<String> fields;
	private int startAt;
	
	public String getJql() 
	{
		return jql;
	}
	
	public void setJql(String jql) 
	{
		this.jql = jql;
	}
	
	public List<String> getFields() 
	{
		return fields;
	}
	
	public void setFields(List<String> fields) 
	{
		this.fields = fields;
	}

	public int getStartAt() 
	{
		return startAt;
	}

	public void setStartAt(int startAt) 
	{
		this.startAt = startAt;
	}
	
	
}
