package eu.ncdc.sumervacation;

import java.io.IOException;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

import javax.mail.MessagingException;

import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.EventAttendee;

import eu.ncdc.summervacation.enties.Vacation;
import eu.ncdc.summervacation.enties.VacationEvents;
import eu.ncdc.summervacation.jiraenties.Issue;
import eu.ncdc.summervacation.jiraenties.Worklog;

public class UserInterface 
{
	private Configs configs;

	private Calendar calendar;
	
	private String name;
	private VacationEvents userVacations;
	private List<Worklog> userWorklogs;
	
	private int currentYear;
	
	public UserInterface(Calendar calendar, Configs configs) throws IOException, ParseException
	{
		this.calendar = calendar;
		this.configs = configs;
		this.name = configs.getUserName();
		
		userVacations = new VacationEvents(calendar, configs);
		
		userVacations.setJiraPassword(configs.getJiraPassword());
		
		currentYear = userVacations.getCurrentYear();
		
		Vacation vacation = new Vacation(configs.getJiraUserName(), configs.getJiraPassword());
		
		vacation.setIssueKey(userVacations.getSubTaskKey(currentYear));
		
		userWorklogs = vacation.getAllVacationWorklogs();
		
		vacation.setIssueKey(userVacations.getSubTaskKey(currentYear + 1));
		
		if (vacation.getIssueKey() != null)
		{
			for (Worklog worklog : vacation.getAllVacationWorklogs())
			{
				GregorianCalendar today = new GregorianCalendar();
				
				today.set(GregorianCalendar.HOUR_OF_DAY, 8);
				
				if (worklog.getStarted().equals(today.getTime()) || worklog.getStarted().after(today.getTime()))
					userWorklogs.add(worklog);
			}
		}
		
		for (Vacation vac : userVacations.getVacationEvents())
		{
			vac.setIssueKey(userVacations.getSubTaskKey(currentYear));
			vac.getWorklogs();
			
			vac.setIssueKey(userVacations.getSubTaskKey(currentYear + 1));
			vac.getWorklogs();
			
			vac.setIssueKey(userVacations.getSubTaskKey(currentYear));
		}
		
		
		
		if (!isSynchronized())
		{
			System.err.println("Your Jira and Google Calendar is not synchronized! Fix it manually!");
		}
	}
	
	public boolean isSynchronized() throws ParseException
	{
		
		for (Vacation vac : userVacations.getVacationEvents())
		{			
			if (!vac.isSynchronized())
				return false;
		}
		
		return true;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public String getName()
	{
		return name;
	}
	
	@SuppressWarnings("deprecation")
	public void addVacation() throws IOException, ParseException, MessagingException
	{
			
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("Podaj dat� startu urlopu: ");
		
		int startYear = scanner.nextInt();
		int startMonth = scanner.nextInt() - 1;
		int startDay = scanner.nextInt();
		
		GregorianCalendar start = new GregorianCalendar(startYear, startMonth, startDay, 8, 0, 0);
		
		System.out.print("Podaj dat� ko�ca urlopu: ");
		
		int endYear = scanner.nextInt();
		int endMonth = scanner.nextInt() - 1;
		int endDay = scanner.nextInt();
		
		GregorianCalendar end = new GregorianCalendar(endYear, endMonth, endDay, 16, 0, 0);
					
		if (end.getTime().before(start.getTime()))
		{
			GregorianCalendar temp = start;
			start = end;
			end = temp;
		}
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSS'Z'");
		
		
		
		Vacation vacation = new Vacation(configs.getJiraUserName(), configs.getJiraPassword());
		
		vacation.setIssueKey(userVacations.getSubTaskKey(currentYear));
		
		vacation.seteMailFrom(configs.getEmailFrom());
		vacation.seteMailTo(configs.getEmailTo());
		vacation.setCalendarID(configs.getCalendarId());
		vacation.setSummary("Vacation " + name + " NCDC");
		vacation.setStart(dateFormat.format(start.getTime()));
		vacation.setEnd(dateFormat.format(end.getTime()));
		
		for (String email : configs.getGuestEmails())
		{
			EventAttendee attendee = new EventAttendee();
			
			attendee.setEmail(email);
			
			vacation.addAttendee(attendee);
		}
		
		ArrayList<Worklog> vacationWorklogs = new ArrayList<Worklog>();
		
		while (true)
		{
			if (start.after(end) && !start.equals(end)) break;
			
			int weekDay = start.get(GregorianCalendar.DAY_OF_WEEK);
			
			if (weekDay != GregorianCalendar.SATURDAY && weekDay != GregorianCalendar.SUNDAY)
			{
				Worklog worklog = new Worklog();
				
				worklog.setComment("Vacation " + name + " NCDC");
				worklog.setStarted(start.getTime());
				worklog.setTimeSpentSeconds(8*3600l);
				
				vacationWorklogs.add(worklog);
			}
			
			start.add(GregorianCalendar.DATE, 1);
			
			start.setTime(start.getTime());
		}
		
		for (Worklog w : vacationWorklogs)
		{	
			int year = 1900 + w.getStarted().getYear();
			
			if (year != currentYear)
			{
				vacation.setIssueKey(userVacations.getSubTaskKey(year));
			}
			
			vacation.addWorklog(w);
		}
		
		userWorklogs.clear();
		
		vacation.setIssueKey(userVacations.getSubTaskKey(currentYear));
		vacation.getWorklogs();
		
		userWorklogs = vacation.getAllVacationWorklogs();
		
		if (userVacations.getSubTasks().size() == 2)
		{
			vacation.setIssueKey(userVacations.getSubTaskKey(currentYear + 1));
			vacation.getWorklogs();
			
			for (Worklog w : vacation.getAllVacationWorklogs())
			{
				userWorklogs.add(w);
			}
		}
		
		vacation.addEvent(calendar);

		userVacations.add(vacation);
		
		vacation.emailSending(name, currentYear, "New vacation: " + vacation.toString());
	}
	
	public void addVacation(String startString, String endString) throws IOException, ParseException, MessagingException
	{	
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSS'Z'");
		
		startString += "T08:00:00.000Z";
		endString += "T16:00:00.000Z";
		
		GregorianCalendar start = new GregorianCalendar();	
		GregorianCalendar end = new GregorianCalendar();
					
		start.setTime(dateFormat.parse(startString));
		end.setTime(dateFormat.parse(endString));
		
		start.set(GregorianCalendar.HOUR_OF_DAY, 8);
		end.set(GregorianCalendar.HOUR_OF_DAY, 16);
		
		//Checking start is before end
		if (end.before(start))
		{
			GregorianCalendar temp = start;
			start = end;
			end = temp;
		}
		
		//Checking start is after today
		if (start.getTime().before(new GregorianCalendar().getTime()))
		{
			System.out.println("Sorry, too late!");
			
			return;
		}
		
		//Checking you have not vacation time between start and end
		if (!Vacation.isDateFree(calendar, configs.getCalendarId(), startString, endString))
		{
			System.out.println("You have one vacation on this time :-)");
			
			return;
		}
		
		Vacation vacation = new Vacation(configs.getJiraUserName(), configs.getJiraPassword());
		
		vacation.setIssueKey(userVacations.getSubTaskKey(currentYear));
		
		vacation.seteMailFrom(configs.getEmailFrom());
		vacation.seteMailTo(configs.getEmailTo());
		vacation.setCalendarID(configs.getCalendarId());
		vacation.setSummary("Vacation " + name + " NCDC");
		vacation.setStart(dateFormat.format(start.getTime()));
		vacation.setEnd(dateFormat.format(end.getTime()));
		
		for (String email : configs.getGuestEmails())
		{
			EventAttendee attendee = new EventAttendee();
			
			attendee.setEmail(email);
			
			vacation.addAttendee(attendee);
		}
			
		vacation.addEvent(calendar);

		userVacations.add(vacation);
		
		ArrayList<Worklog> vacationWorklogs = new ArrayList<Worklog>();

		Date endDate = end.getTime();
		
		endDate.setHours(16);
		
		while (true)
		{
			Date startDate = start.getTime();
							
			if (startDate.after(endDate)) break;
			
			int weekDay = start.get(GregorianCalendar.DAY_OF_WEEK);
			
			if (weekDay != GregorianCalendar.SATURDAY && weekDay != GregorianCalendar.SUNDAY)
			{
				Worklog worklog = new Worklog();
				
				String weekDayString = null;
				
				switch (weekDay)
				{
					case GregorianCalendar.MONDAY:
						weekDayString = "Monday";
						break;
						
					case GregorianCalendar.TUESDAY:
						weekDayString = "Tuesday";
						break;
						
					case GregorianCalendar.WEDNESDAY:
						weekDayString = "Wednesday";
						break;
						
					case GregorianCalendar.THURSDAY:
						weekDayString = "Thursday";
						break;
						
					case GregorianCalendar.FRIDAY:
						weekDayString = "Friday";
						break;
						
					case GregorianCalendar.SATURDAY:
						weekDayString = "Saturday";
						break;
						
					case GregorianCalendar.SUNDAY:
						weekDayString = "Sunday";
						break;
				}
				
				worklog.setComment("Vacation " + name + " NCDC " + weekDayString);
				worklog.setStarted(start.getTime());
				worklog.setTimeSpentSeconds(8*3600l);
				
				vacationWorklogs.add(worklog);
			}
			
			start.add(GregorianCalendar.DATE, 1);
			
			start.setTime(start.getTime());
		}
		
		for (Worklog w : vacationWorklogs)
		{			
			int year = 1900 + w.getStarted().getYear();
			
			if (year != currentYear)
			{
				vacation.setIssueKey(userVacations.getSubTaskKey(year));
			}
			
			vacation.addWorklog(w);
		}
		
		userWorklogs.clear();
		
		vacation.setIssueKey(userVacations.getSubTaskKey(currentYear));
		vacation.getWorklogs();
		
		userWorklogs = vacation.getAllVacationWorklogs();
		
		if (userVacations.getSubTasks().size() == 2)
		{
			vacation.setIssueKey(userVacations.getSubTaskKey(currentYear + 1));
			vacation.getWorklogs();
			
			for (Worklog w : vacation.getAllVacationWorklogs())
			{
				userWorklogs.add(w);
			}
		}
		vacation.emailSending(name, currentYear, "New vacation: " + vacation.toString());
	}

	
	public void deleteVacation() throws ParseException, MalformedURLException, IOException, MessagingException
	{
		if (userVacations.getNumberOfVacations() == 0)
		{
			System.out.println("You have no planned vacations :-(");
			
			return;
		}
		
		System.out.println("Wklep numer urlopu: ");
		
		Scanner scanner = new Scanner(System.in);
		
		int vacNumber = scanner.nextInt();
		
		vacNumber--;
		
		Vacation delVacation = userVacations.getEvent(vacNumber);
		String idDelVacation = delVacation.getEventID();
		
		delVacation.setIssueKey(userVacations.getSubTaskKey(currentYear));
		delVacation.deleteWorklogs();
		
		if (userVacations.getSubTasks().size() == 2)
		{
			delVacation.setIssueKey(userVacations.getSubTaskKey(currentYear + 1));
			delVacation.deleteWorklogs();
		}
		userWorklogs.clear();
		
		delVacation.setIssueKey(userVacations.getSubTaskKey(currentYear));
		userWorklogs = delVacation.getJiraResource().getWorklogs();
		
		if (userVacations.getSubTasks().size() == 2)
		{
			delVacation.setIssueKey(userVacations.getSubTaskKey(currentYear + 1));
			
			for (Worklog w : delVacation.getJiraResource().getWorklogs())
			{
				userWorklogs.add(w);
			}
		}
		userVacations.remove(idDelVacation);
		
		delVacation.emailSending(name, currentYear, "Delete vacation: " + delVacation.toString());
	}
	
	public void deleteVacation(int index) throws ParseException, MalformedURLException, IOException, MessagingException
	{
		if (userVacations.getNumberOfVacations() == 0)
		{
			System.out.println("You have no planned vacations :-(");
			
			return;
		}
						
		Vacation delVacation = userVacations.getEvent(index);
		String idDelVacation = delVacation.getEventID();
		
		delVacation.setIssueKey(userVacations.getSubTaskKey(currentYear));
		delVacation.deleteWorklogs();
		
		if (userVacations.getSubTasks().size() == 2)
		{
			delVacation.setIssueKey(userVacations.getSubTaskKey(currentYear + 1));
			delVacation.deleteWorklogs();
		}
		userWorklogs.clear();
		
		delVacation.setIssueKey(userVacations.getSubTaskKey(currentYear));
		userWorklogs = delVacation.getJiraResource().getWorklogs();
		
		if (userVacations.getSubTasks().size() == 2)
		{
			delVacation.setIssueKey(userVacations.getSubTaskKey(currentYear + 1));
			
			for (Worklog w : delVacation.getJiraResource().getWorklogs())
			{
				userWorklogs.add(w);
			}
		}
		userVacations.remove(idDelVacation);
		
		delVacation.emailSending(name, currentYear, "Delete vacation: " + delVacation.toString());
	}
	
	public void deleteAllWorklogs() throws MalformedURLException, IOException, ParseException
	{
		for (Issue issue : userVacations.getSubTasks())
		{
			Vacation vacation = new Vacation(configs.getJiraUserName(), configs.getJiraPassword());
			
			vacation.setIssueKey(issue.getKey());
			
			for (Worklog w : userWorklogs)
			{
				vacation.deleteWorklog(w.getId());
			}
			
		}		
		
		userWorklogs.clear();
		
		
	}
	
	public void deleteAllEvents() throws IOException, ParseException
	{
		for (Vacation vac : userVacations.getVacationEvents())
		{
			vac.deleteEvent(calendar);
		}
		
		userVacations.getVacationEvents().clear();
	}
	
	public void showVacations()
	{
		userVacations.showVacations();
	}
	
	public void interf() throws IOException, ParseException, MessagingException
	{
		Scanner scanner = new Scanner(System.in);
		
		while (true)
		{	
			showVacations();
			
			System.out.print("\nWklep liczb�: ");
			
			int choice = scanner.nextInt();
			
			if (choice == 0) break;
			
			switch (choice)
			{
				case 0:
					break;
					
				case 1:
					addVacation();
					break;
					
				case 2:
					deleteVacation();
					break;
					
				case 3:
					deleteAllWorklogs();
					break;
					
				case 4:
					deleteAllEvents();
					break;
					
				case 5:
					isSynchronized();
					break;
					
			}
		}
		
		scanner.close();
	}

	public int getCurrentYear() 
	{
		return currentYear;
	}

	public void setYear(int currentYear) 
	{
		this.currentYear = currentYear;
	}
	
	public Configs getConfigs() 
	{
		return configs;
	}

	public void setConfigs(Configs configs) 
	{
		this.configs = configs;
	}

	public VacationEvents getUserVacations() {
		return userVacations;
	}

	public void setUserVacations(VacationEvents userVacations) {
		this.userVacations = userVacations;
	}
		
}
